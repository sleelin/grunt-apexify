/*
 * grunt-apexify
 * https://bitbucket.org/sleelin/apexify
 *
 * Copyright (c) 2015 S. Lee-Lindsay
 * Licensed under the GNU license.
 */

"use strict";

var path = require("path"),
    Apexify = require("apexify");

module.exports = function(grunt) {

    grunt.registerMultiTask("apexify", "Translate standard HTML pages into APEX, the SalesForce VisualForce page definition language", function() {
        var done = this.async(),
            apexifier = new Apexify(),
            opts = this.options({
                pageSource: "page.html"
            });

        this.files.forEach(function (f, index, files) {
            if ((grunt.file.isMatch("**/pages", f.dest) || (grunt.file.isMatch("**/pages/**", grunt.file.expand({cwd: f.orig.cwd, filter: "isDirectory"}, f.src))))) {
                grunt.file.expand({cwd: f.cwd, filter: "isDirectory"}, f.src).forEach(function (src) {
                    apexifier.processPage(grunt.file.read([f.cwd, src, opts.pageSource].join("/"))).then(function (page) {
                        grunt.file.write([f.dest, [src, "page"].join(".")].join("/"), page);
                    });
                });
            } else if (path.extname(f.orig.dest) === ".zip") {
                var bundle = apexifier.addBundle(f.orig.dest);

                (!!f.orig.expand ? f.src : grunt.file.expand({cwd: f.orig.cwd, filter: "isFile"}, f.src)).map(function (src) {
                    return (!!f.orig.expand ? src.replace(f.orig.cwd, "").substr(1) : src);
                }).forEach(function (src) {
                    bundle.consider(src, grunt.file.read([f.orig.cwd, src].join("/")), !!f.orig.expand);
                });
            } else {
                apexifier.addResource(f.src, grunt.file.read(f.src));
            }

            if (index === files.length-1) {
                apexifier.resolveDependencies(function (res) {
                    grunt.file.write(res.dest, res.get());
                }).finally(done);
            }
        });
    });

};
